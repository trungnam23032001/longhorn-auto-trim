FROM python:3.10-slim as builder

WORKDIR /app

COPY requirements.txt .

RUN pip install -r requirements.txt

FROM cgr.dev/chainguard/python:3.10

WORKDIR /app

# Make sure you update Python version in path
COPY --from=builder /usr/local/lib/python3.10/site-packages /home/nonroot/.local/lib/python3.10/site-packages

COPY auto_trim_api.py longhorn.py /app/

ENTRYPOINT ["python", "auto_trim_api.py"]