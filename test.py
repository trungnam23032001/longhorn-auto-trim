import logging
logging.basicConfig(level=logging.DEBUG, 
                    format='ts="%(asctime)s" level="%(levelname)s" msg="%(message)s"',
                    datefmt='%Y-%m-%dT%H:%M:%SZ')
logging.info('This message will be logged')
logging.debug('This message will not be logged')